# Module Pricing Manager

## Rôle

Ce module est un module qui s'occupe de gérer tout ce qui est relatif à la gestion des tarif des produits fournis par le cluster

## Dépendances



## Service mis à disposition

Un service est mis à disposition pour les modules providers pour leur permettre de configurer les tarifs qui leur sont appliqué

### Fonctionnalité commune

Chaque service de tarification permettent de configurer et stocker les tarifs des différentes ressource. Ils permettent aussi de configurer des tarifs spécifique en fonction du/des partenaires/groupes

Ces services peuvent être interrogé 

### Service de tarification data

Aide les différents provider de data à gérer les différents tarif appliqué à leurs données. Les tarifs peuvent être appliqué selon différent critères:
* Taille de l'image
* Superficie de la zone couverte
* À l'image (peu importe la taille/superficie)


### Service de tarification compute

Aide le provider compute à gérer les différents tarif appliqué. Les tarifs peuvent être appliqué selon différent critères:
* prix/perf CPU
* prix/RAM
* prix/GPU
* prix/classe stockage
### Service de tarification traitement

Aide les différents provider de services à gérer les différents tarif appliqué à leurs traitements. Les tarifs peuvent être appliqué selon différent critères:
* Nombre de données traité
* Relatif aux caractéristiques des données traité (voir tarif data)

### Service de tarification storage


### Service de tarification workflow

Aide le provider de workflows à gérer les différents tarif appliqué à leurs traitements. Les tarifs peuvent être appliqué selon différent critères:
* Nombre de données traité
* Relatif aux caractéristiques des données traité (voir tarif data)




