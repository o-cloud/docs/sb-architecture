# Module Compute Provider

## Rôle

Ce module s'occupe de définir les capacité de calcul qui peuvent être mis à disposition des autres cluster du réseau et de surveiller l'utilisation des ressources

## Dépendances

Ce module s'appuie sur le module Pricing Manager pour gérer la tarification. Sur l'instance Kubecost pour le suivit de la consommation et des coûts associé.

## Service mis à disposition

2 service sont mis à disposition. Un service interne pour la configuration des ressources de computing mis à disposition et le monitoring des ressources utilisée. Et un service externe qui permettra à d'autres clusters d'obtenir plus d'informations sur les ressource (comparé au métadonnées catalogue), d'effectuer des réservation et de préparer de lancement de workflow

### Service interne

Ce service permet de configurer les capacité de calcul (compute) mis à disposition par le cluster.

Il permet de définir quels noeuds du cluster sont mis à disposition et leur tarifications (appuyé par le module Pricing Manager).

Il aura aussi un rôle de contrôle sur les pods qui seront démarré:
* s'assurer qu'ils respectent bien la réservation qui à été effectué au préalable
* s'assurer que les métadonnée de suivit pour Kubecost soient bien présent

### Service externe

