# Module

## Rôle

Ce module s'occupe de définir les ressources de type données qui peuvent être mis à disposition des autres cluster du réseau.

Un module spécifique est à crée pour chaque type de sources de données. Il aura pour but de rendre compatible la source de données avec les workflow/gestion des données du réseau.

## Dépendances


Ce module s'appuie sur le module Pricing Manager pour gérer la tarification. 
Il doit avoir accès au système de stockage sous jacent de la donnée.

## Service mis à disposition

2 service sont mis à disposition. Un service interne pour la configuration des ressources. Et un service externe qui permettra à d'autres clusters d'obtenir plus d'informations sur les ressource (comparé au métadonnées catalogue), d'effectuer des achats/réservation pour l'utilisation dans un workflow.

### Service interne

Ce service permet de configurer les sources données mis à disposition par le cluster. À l'aide du pricing manager, il permet aussi de définir la tarification de l'utilisation des données.

### Service externe

Ce service permet au cluster externes d'effectuer des réservations/achats de données pour être récupéré et/ou exploité dans des traitements. 

Il peut aussi faire office de plateforme d'accès au données pour ajouter une couche de monitoring à des systèmes qui n'en disposent pas.