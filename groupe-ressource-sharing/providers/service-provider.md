# Module

## Rôle

Ce module s'occupe de définir les services/traitements qui peuvent être mis à disposition des autres cluster du réseau.

Les services/traitement sont majoritairement mis à disposition sous forme de conteneurs.

## Dépendances

Ce module s'appuie sur le module Pricing Manager pour gérer la tarification.

Il doit avoir accès à un système de stockage des images.


## Service mis à disposition


2 service sont mis à disposition. Un service interne pour la configuration des ressources. Et un service externe qui permettra à d'autres clusters d'obtenir plus d'informations sur les ressource (comparé au métadonnées catalogue), d'effectuer des achats/reservation pour l'utilisation dans un workflow.

### Service interne

Ce service permet de configurer les traitements mis à disposition par le cluster.

Il doit aussi être capable de gérer le versionning des images

A l'aide du Pricing manager, il permet aussi de définir la tarification de l'accès au images.

### Service externe

Ce service permet au cluster externes d'effectuer des réservations/achats des images de traitement pour être exploité dans des traitements. 
Il peut aussi faire office de système intermédiaire pour l'accès au traitements pour le déploiement d'un workflow.