# Module

## Rôle

Ce module permet de mettre à disposition des workflow déjà crée et configuré à d'autres membres du cluster.
Il permet aussi de gérer leur executions
## Dépendances

Ce module s'appuie sur le module Pricing Manager pour gérer la tarification. 
Il doit avoir accès au module de gestion de workflow

## Service mis à disposition

2 service sont mis à disposition. Un service interne pour la configuration des workflow qui sont mis à disposition. Et un service externe qui permettra à d'autres clusters d'obtenir plus d'informations sur les workflow (comparé au métadonnées catalogue), de récupérer un workflow ou de l'
exécuter un workflow sur le cluster.

### Service interne

Ce service permet de configurer les sources données mis à disposition par le cluster. À l'aide du pricing manager, il permet aussi de définir la tarification de l'utilisation des données.

### Service externe

Ce service permet au cluster externes de récupérer ou d'exécuter un workflow.
