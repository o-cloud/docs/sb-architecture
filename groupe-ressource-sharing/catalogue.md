# Module

## Rôle

Ce module  fait office de point d'entrée pour la recherche de ressources dans le réseau.

Il s'occupe de répondre au recherche des clusters (local ou distants) en prenant en compte les relations configurée avec l'Identity Manager et les prix determiné par Pricing Manager

## Dépendances

Un accès vers une base ElasticSearch où sont indexé les métadonnées des produits proposée.

Il doit pouvoir consulter le module Identity Manager pour avoir accès au niveau d'accès qui sera accordé au cluster qui fait la recherche

Il doit pouvoir communiquer avec le module Pricing Manager pour pouvoir déterminer quel sera le tarif appliqué au produits issu de la recherche en fonction du cluster appelant (un produit peut avoir plusieurs tarif). 

## Service mis à disposition

Une API de recherche sur le catalogue est mis à disposition
### Catalogue

Un URL accessible depuis l'extérieur (et éventuellement l'intérieur) du cluster est mis à disposition pour effectuer une recherche

Le service de recherche permet d'effectuer une recherche sur tout les types de produits mis à disposition par le cluster (data, computing, traitements etc).

Lors d'une recherche on doit être capable d'interpréter les critères de recherche :
* texte dans la description et/ou titre
* compatibilité avec d'autres composant (format de donnée entre traitements et source de donnée, ressource de computing entre traitement et capacité de calcul)

Les recherche sont effectué sur ElasticSearch qui contient uniquement les métadonnée des ressources, puis les tarif sont appliqué une fois les résultats sont récupéré depuis ElasticSearch. 
