


## Traitement en 3 étapes

* Définition du workflow
* Configuration de l'exécution
* (Monitoring)
* Récupération des données
  
### Définition du workflow

Organisation du workflow

* Choix des briques 
  * Source de données
  * Traitements
  * Destinations
* Flow d'exécution
  * Liaison entre les entrée sortie des briques
  * Comportement si étape OK ou KO
  * Valeurs propre a l'algorithme (limite de couverture nuageuse)

### Configuration de l'exécution



* Configuration de paramètres dynamiques
  * Zone couverte par les source de donnée
  * Intervalle de temps à traité (du x au x, 15 deniers jours, le mois précédent)
* Date d'exécution (immédiat ou différé)
* Périodicité (Exécuter tout les Semaines)

### Récupération

* Récupération des données en fin de traitement dans un système de stockage du cluster (Dossier, Object storage, etc)

## Diagrammes de séquence

### Préparation

```mermaid
sequenceDiagram
    participant U as Utilisateur
    participant L as Local
    participant D as Data provider
    participant C as Computing Provider
    participant S as Service Provider
    U->>L: Déploiement pipeline
    par data
    L->>D: Reservation des données
    D-->>L: Bond de reservation
    and compute
    L->>C: Reservation des Capacité de calcul
    C-->>L: Bond de reservation
    and traitements
    L->>S: Reservation des Traitements
    S-->>L: Bond de reservation
    end
```


### Déploiement du workflow

```mermaid
sequenceDiagram
    participant L as Local
    participant D as Data Provider
    participant W as Workflow Argo (C. Computing)
    participant S as Service provider
    
    L->>L: Déploiement du workflow
    activate L
    L->>+W: Propagation des pods (MCS)
    W-->>S: Récupération des conteneurs
    loop le  argo + MultiClusterScheduler
        W-->>D: Récupération des données
        W-->>W: Execution du workflow
        L-->>W: Monitoring execution
    end
    W->>L: Envoie des resultats
    deactivate W
    deactivate L
```

