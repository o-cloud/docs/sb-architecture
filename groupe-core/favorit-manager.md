# Module

## Rôle

Ce module s'occupe de gérer la liste de favoris des utilisateurs

## Dépendances


## Service mis à disposition

Ce module met à disposition des services permettant la gestion de ressources "favorite"

Les opérations classique d'enregistrement/mise à jour, lecture et suppression.

Ces favoris peuvent être à différent niveau : utilisateur, groupe ou cluster (global)

Ce module va aussi avoir un rôle de vérification des ressources dans les cluster distant pour s'assurer que la ressource est toujours disponible et aussi vérifier si il y a de nouvelles version de la ressource de disponible.


