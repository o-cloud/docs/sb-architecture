# Module Core

## Rôle

Ce micro service est en lien directe avec K8S, il s'occupe de gérer les autres micro services dans K8s (déploiement, configuration, réplications etc...) via Helm.

Il est déployé en premier dans le cluster, et s'occuper d'installer et de faire la configuration initial des autres modules nécessaires pour disposer de la configuration minimal pour participer au réseau.

## Dépendances

Ce module a besoins d'une connexion a une base de donnée pour stocker son état.

Il utilise Helm Charts pour le déploiement des modules.
## Service mis à disposition
Met a disposition des API permettant de consulter et contrôler l'état du cluster

Ces API sont disponible uniquement depuis l'intérieur du cluster.

### Déploiement des modules sur le cluster
Il doit être capable d'interagir avec Kubernetes via Helm Charts pour déployer ensemble des modules ainsi que de récupérer l'état de ces modules dans le cluster Kubernetes

Un Chart Helm de base sera crée représentant l'ensemble des déploiement possible

Cette API permet de récupérer l'état et statistiques d'exécution des modules présents

Cette API permet aussi de configurer l'état voulu du cluster, ainsi que les modules qui doivent être présent (Présence ou non du groupe d'exécution de workflow par exemple)

## Sécurité

Seul les utilisateurs qui sont considérer comme administrateurs du cluster peuvent interagir avec ce module.

