# Module Discovery

## Rôle

Ce module s'occupe de la découverte des paires à travers un ou plusieurs réseau IPFS

## Dépendances

Les pairs détecté sont enregistré dans des éléments K8S, il doit donc avoir accès à l'API K8S pour les enregistrer

## Service mis à disposition


### Gestion des réseaux IPFS et des identité dans les réseaux IPFS

Ce service permet de contrôler à quels réseaux IPFS le module Discovery doit se connecter. Ainsi un cluster peut décider de créer son propre réseau pour lui et ces clusters sans avoir besoins de rejoindre le réseau par défaut.

Il permet de configurer quels informations sont propagé dans le réseau IPFS pour pouvoir être découvert par les autres clusters

Les informations des autres clusters découvert sont enregistré dans kubernetes

Les informations qui sont propagé dans IPFS contiennent à minima:
* Un moyen d'authentification (Clé publique?)
* Un nom
* L'adresse (Ip ou URL) d'accès à l'API publique du projet SB


### ??? Gestion de connexion manuel vers des partenaires 

Possibilité d'ajouter directement un partenaire sans passer par un réseau IPFS