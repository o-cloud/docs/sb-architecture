# Module

## Rôle

Ce module met à disposition l'interface d'utilisation et d'administration du projet SB

## Dépendances

Ce module dépend de tout les modules qui peuvent être présent dans le cluster

## Service mis à disposition

Ce module est divisé en 2 partie distincte :
* Un serveur back-end qui servira de middleware entre le front et les autres modules du cluster
* L'application Web (Single Page App en Angular2)

### Serveur back-end

L'application front s'appuie sur un serveur back qui s'occupe de gérer les droits utilisateurs et de faire office d'intermédiaire entre l'application front et les modules du cluster local ainsi que les services mis à disposition par les clusters externes (providers ou catalogue par exemple).

### Application front 

L'application front fait office d'interface d'accès pour l'utilisateur à l'ensemble des fonctionnalité du réseau SB.