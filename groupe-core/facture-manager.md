# Module

> Ce module est utile dans le cas où si l'on utilise pas encore de Blockchain avec SmartContract

## Rôle

Ce module s'occupe de regrouper toutes les facture émise par les différents provider lors de consommation de ressources (que ça soit directement depuis l'interface ou lors de workflow)

## Dépendances

Besoins d'accès à la base de donnée pour enregistrer les factures.
Doit pouvoir exposer des routes externes
## Service mis à disposition

Ce module met à disposition plusieurs service autour des factures
* Réception
* Émission
* Consultation (interne au cluster)

### Réception de factures

Un service est mis à disposition pour la réception de facture.
Ce service est accessible depuis l'extérieur.

Lorsque l'on engage une consommation de ressource, on prépare une réception de facture (une ou plusieurs). 

Pour une ressource type computing, envoie la facture une fois le calcul effectué.
Pour une ressource de type donnée, la facture est envoyer avant ou en même temps que les données
### Émission de factures

Un service est mis à disposition pour l'émission de de facture.

Lorsque l'on met à disposition des ressources, une facture est crée (le prix étant déterminer par le provider et par le module Pricing Manager).
Puis cette facture est émise vers le cluster consommateur de la ressource une fois le calcul distribué.

### Consultation des factures

Un service met à disposition les factures émise vers et les factures reçue par les autres clusters.
Principalement pour le front.
