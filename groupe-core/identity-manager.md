# Module Identity Manager

## Rôle

Ce module gère tout ce qui est relatif au identité des différents acteurs du réseau, ainsi que la gestion des droits de ces acteurs sur le cluster.

Les acteurs à gérer sont:
 * Les utilisateurs du cluster local
 * Les clusters (Organisations)
 * Des groupements de cluster
 * (plus tard) Les utilisateurs des clusters distant
 * (plus tard) Les utilisateurs rattaché au groupes de cluster


## Dépendances
Ce module a besoins d'une connexion à une base de donnée pour stocker son état.

Il doit aussi avoir accès au module qui s'occupe de la découverte des réseau


## Service mis à disposition
Met à disposition des API permettant de consulter l'identité et droits des différents acteurs.

Ces API sont disponible uniquement depuis l'intérieur du cluster.


### Authentification et autorisation des utilisateurs

Service simple d'authentification des utilisateurs. 
* Gestion user/mot de passe. 
* Gestion de la liste de droits (ABAC? ACL?)

Ce service est accessible uniquement depuis l'intérieur du cluster


### Gestion des identité des autres clusters

Service de gestion des identité des clusters distants (et de leurs utilisateurs) qui sont retourné par le module de découverte des réseaux

Un process fait en continue est d'interroger les clusters connu pour avoir plus d'informations sur eux notamment les ressources qu'ils pourraient mettre à disposition. Les autres modules du cluster local peuvent ainsi interroger le module Identity Manager pour avoir des informations sur les clusters distants. (Ex: le module front, lors de la recherche, demande la liste des cluster qui mettent à disposition des sources de données ou des traitements)


### Gestion des groupes de clusters et clusters de confiance

Il est important de pouvoir déterminer des clusters distants de confiance qui auront des accès privilégié au ressources mis à disposition par le cluster local

On peut aussi gérer des groupes de clusters pour simplifier les accès privilégié (regroupement d'acteurs autours d'un projet commun par exemple)

Cette gestion peut être spécifique 
* au cluster local: les groupes sont définit localement et les autres clusters n'en n'ont pas conscience)
* au groupe de clusters: partager entre les clusters, les clusters partage leur configuration qui se propage aux autres, et un système de consensus pour l'ajout de nouveau membre doit être mis en place


### Gestion de son identité et capacité auprès des autres cluster

Indique aux autres clusters qui en font la demande ce que le ce cluster met à disposition comme ressource. Les informations communiqué au autres cluster peuvent être configuré en fonction du niveau de confiance du cluster appelant

Permet aux autres clusters de confirmer l'identité de ce cluster dans les messages qui leur serait transmit par d'autres clusters.


### (Plus tard) Gestion fine des utilisateurs appartenant au  utilisateurs distant

De la même manière que des accès privilégié au ressource locales peuvent être fournis à un ou plus clusters distant, cette fois ci il est possible de spécifier un utilisateur ou groupe d'utilisateur du cluster distant.